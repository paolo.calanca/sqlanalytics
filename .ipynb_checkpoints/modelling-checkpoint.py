import pickle
import numpy as np
import pandas as pd
from pandas import DataFrame
from pandarallel import pandarallel
from sklearn import metrics

import sqlquery_metadata
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt



def load_training_data(filepath="data/slow_log.json"):
    df = pd.read_json("data/slow_log.json", convert_dates=False)
    # df.start_time = pd.to_datetime(df.start_time)
    df.query_time = pd.to_timedelta(df.query_time).dt.total_seconds()
    # return df[['query_time', 'rows_examined', 'sql_text']]
    return df[['query_time',  'sql_text']]


def numeric_features(df: DataFrame):
    pandarallel.initialize()
    df['tables'] = df.sql_text.parallel_apply(sqlquery_metadata.query_tables)
    df.dropna(subset=['tables'], inplace=True)
    df['n_tables'] = df['tables'].apply(len)
    df['n_columns'] = df.sql_text.parallel_apply(sqlquery_metadata.n_query_columns)
    df['columns_where'] = df.sql_text.parallel_apply(sqlquery_metadata.query_columns_where_cond)
    df['n_columns_where'] = df['columns_where'].apply(len)
    df['n_keycols_where'] = df['columns_where'].parallel_apply(lambda x : len(set(x).intersection(sqlquery_metadata.KEY_COLUMNS)))
    df['n_nonkeycols_where'] = df['n_columns_where'] - df['n_keycols_where']
    df['n_groupby'] = df.sql_text.parallel_apply(sqlquery_metadata.query_groupby_conditions)
    df['n_orderby'] = df.sql_text.parallel_apply(sqlquery_metadata.query_orderby_conditions)
    df.drop(columns=['tables', 'sql_text', 'columns_where', 'n_columns_where'], inplace=True,  errors='ignore')
    return df


def all_features(df: DataFrame):
    pandarallel.initialize()
    df['tables'] = df.sql_text.parallel_apply(sqlquery_metadata.query_tables)
    df.dropna(subset=['tables'], inplace=True)
    df['n_tables'] = df['tables'].apply(len)
    for t in sqlquery_metadata.TABLES:
        df['table_'+t] = df['tables'].parallel_apply(lambda x: 1 if t in x else 0)

    df['columns'] = df.sql_text.parallel_apply(sqlquery_metadata.query_columns)
    df['n_columns'] = df['columns'].apply(len)
    for c in sqlquery_metadata.COLUMNS:
        df['col_'+c] = df['columns'].parallel_apply(lambda x: 1 if c in x else 0)

    df['columns_where'] = df.sql_text.parallel_apply(sqlquery_metadata.query_columns_where_cond)
    df['n_columns_where'] = df['columns_where'].apply(len)
    df['n_keycols_where'] = df['columns_where'].parallel_apply(lambda x: len(set(x).intersection(sqlquery_metadata.KEY_COLUMNS)))
    df['n_nonkeycols_where'] = df['n_columns_where'] - df['n_keycols_where']
    for c in sqlquery_metadata.COLUMNS:
        df['where_col_'+c] = df['columns_where'].parallel_apply(lambda x: 1 if c in x else 0)

    df['n_groupby'] = df.sql_text.parallel_apply(sqlquery_metadata.query_groupby_conditions)
    df['n_orderby'] = df.sql_text.parallel_apply(sqlquery_metadata.query_orderby_conditions)
    df.drop(columns=['tables', 'sql_text', 'columns', 'columns_where'], inplace=True)
    return df


def split_data(df: DataFrame, test_size=0.2):
    X = df.drop(columns=['query_time']).to_numpy()
    y = df['query_time'].to_numpy()
    return train_test_split(X , y, test_size=test_size, shuffle=True, random_state=0)


def evaluate_model(model, X_train, X_test, y_train, y_test):
    model.fit(X_train, y_train)
    y_pred_train = model.predict(X_train)
    train_score = metrics.mean_squared_error(y_train, y_pred_train, squared=False)
    train_err = y_train - y_pred_train

    y_pred_test = model.predict(X_test)
    test_score = metrics.mean_squared_error(y_test, y_pred_test, squared=False)
    test_err = y_test - y_pred_test
    return train_err, test_err, train_score, test_score


def plot_error(train_err, test_err):
    fig, (ax1, ax2) = plt.subplots(2, 1)
    ax1.plot(train_err)
    ax1.set_ylabel('train_err')
    ax2.plot(test_err)
    ax2.set_xlabel('sample')
    ax2.set_ylabel('test_err')
    plt.show()


def load_model():
    with open('my_model/scaler.pkl', 'rb') as file:
        scaler = pickle.load(file)
    with open('my_model/regressor.pkl', 'rb') as file:
        regressor = pickle.load(file)

    return scaler, regressor


def predict(query: str, scaler, regressor):
    # extract features
    X = np.zeros((1, 6))
    X[0, 0] = len(sqlquery_metadata.query_tables(query))
    X[0, 1] = sqlquery_metadata.n_query_columns(query)
    columns_where = sqlquery_metadata.query_columns_where_cond(query)
    n_columns_where = len(columns_where)
    X[0, 2] = len(set(columns_where).intersection(sqlquery_metadata.KEY_COLUMNS))
    X[0, 3] = n_columns_where - X[0, 2]
    X[0, 4] = sqlquery_metadata.query_groupby_conditions(query)
    X[0, 5] = sqlquery_metadata.query_orderby_conditions(query)

    X = scaler.transform(X)
    y = regressor.predict(X)
    return y


if __name__ == '__main__':
    sql_text = """
        select
    	ps_partkey,
    	sum(ps_supplycost * ps_availqty) as value
    from
    	partsupp,
    	supplier,
    	nation
    where
    	ps_suppkey = s_suppkey
    	and s_nationkey = n_nationkey
    	and n_name = 'FRANCE'
    group by
    	table.ps_partkey,
    	s_suppkey
    having
    		sum(ps_supplycost * ps_availqty) > (
    			select
    				sum(ps_supplycost * ps_availqty) * 0.0001000000
    			from
    				partsupp,
    				supplier,
    				nation
    			where
    				ps_suppkey = s_suppkey
    				and s_nationkey = n_nationkey
    				and n_name = 'FRANCE'
    			group by
    			    partsupp.s_suppkey
    		)
    order by
    	value desc;"""

    scaler, regressor = load_model()
    y = predict(sql_text, scaler, regressor)
    print(y)
    # df = load_training_data(filepath="data/slow_log.json")
    # f = all_features(df)
    # features = numeric_features(df)
    # print(features.info())

    # features = pd.read_csv("data/numeric_features.csv")
    #
    # X_train, X_test, y_train, y_test = split_data(features)
    #
    # scaler = StandardScaler().fit(X_train)
    # X_train = scaler.transform(X_train)
    # X_test = scaler.transform(X_test)
    #
    # parameters = {'weights': ['uniform', 'distance'],
    #               'n_neighbors': [1, 2, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50]}
    # clf = GridSearchCV(neighbors.KNeighborsRegressor(), parameters, cv=None, scoring='neg_mean_absolute_error',
    #                    n_jobs=1, return_train_score=True, verbose=100)
    # results = clf.fit(X_train, y_train)
    # print(results)
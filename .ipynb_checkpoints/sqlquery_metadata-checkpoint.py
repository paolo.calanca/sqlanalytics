import sql_metadata, sqlparse

TABLES = ['customer', 'lineitem', 'nation', 'orders', 'part', 'partsupp', 'region', 'supplier']

COLUMNS = {'customer': ['c_custkey', 'c_mktsegment', 'c_nationkey', 'c_name', 'c_address', 'c_phone', 'c_acctbal', 'c_comment']}

KEY_COLUMNS = {'c_custkey', 'c_nationkey', 'l_orderkey', 'l_linenumber', 'l_suppkey', 'l_partkey', 'n_nationkey', 'n_regionkey', 'o_orderkey', 'o_custkey', 'p_partkey', 'ps_partkey', 'ps_suppkey', 'r_regionkey', 's_suppkey', 's_nationkey'}

def query_command(query: str):
    return sql_metadata.get_query_tokens(query)[0].value

def query_tables(query: str):
    return [t for t in sql_metadata.get_query_tables(query) if t in TABLES]
### Data Processing

I implemented the data processing in the 'sqlquery_metadata.py' file

### Data Analytics

The Data Analysis is in the 'analytics.ipynb' jupyter notebook, it uses the 'sqlquery_metadata.py' script.

### Model Training

The Model Selection and Model Training are in the 'model_selection.ipynb' jupyter notebook, it uses the 'sqlquery_metadata.py' script and the 'modelling.py' which implements data loading, features extraction and model evaluation

### Model Deploy

The Model Deploy is implemented in the deploy_model.ipynb notebook. It uses the data in the 'container' folder which contains the serialized models and all the files used to deploy the model on AWS Sagemaker.


Note: The file 'requirements.txt' contains the required libraries to run the notebooks, you need to install them with pip, then you can run the notebooks using the command 'jupyter notebook' from the root folder. 
I have also exported the notebooks in HTML format.
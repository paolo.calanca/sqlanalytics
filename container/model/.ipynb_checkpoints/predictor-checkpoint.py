from __future__ import print_function

import os
import json
import pickle
import io
import sys
import signal
import traceback
import numpy as np
import flask
import sqlquery_metadata
import pandas as pd

model_path = '/opt/ml/'

# A singleton for holding the model. This simply loads the model and holds it.
# It has a predict function that does a prediction based on the model and the input data.

class ScoringService(object):
    scaler = None                # Where we keep the model when it's loaded
    regressor = None

    @classmethod
    def get_model(cls):
        """Get the model object for this instance, loading it if it's not already loaded."""
        if cls.scaler == None:
            with open(os.path.join(model_path, 'scaler.pkl'), 'rb') as file:
                cls.scaler = pickle.load(file)
        if cls.regressor == None:
            with open(os.path.join(model_path, 'regressor.pkl'), 'rb') as file:
                cls.regressor = pickle.load(file)
        return cls.scaler, cls.regressor
    
    @classmethod
    def extract_features(cls, query: str):
        X = np.zeros((1, 6))
        tables = sqlquery_metadata.query_tables(query)
        if tables is None:
            X[0, 0] = 0
        else:
            X[0, 0] = len(tables)        
            
        X[0, 1] = sqlquery_metadata.n_query_columns(query)
        columns_where = sqlquery_metadata.query_columns_where_cond(query)
        n_columns_where = len(columns_where)
        X[0, 2] = len(set(columns_where).intersection(sqlquery_metadata.KEY_COLUMNS))
        X[0, 3] = n_columns_where - X[0, 2]
        X[0, 4] = sqlquery_metadata.query_groupby_conditions(query)
        X[0, 5] = sqlquery_metadata.query_orderby_conditions(query)
        return X

    
    @classmethod
    def predict(cls, query: str):
        """For the input, do the predictions and return them.

        Args:
            input: a query string"""
        
        X = cls.extract_features(query)
        scaler, regressor = cls.get_model()
        
        X = scaler.transform(X)
        y = regressor.predict(X)
        return y

    
# The flask app for serving predictions
app = flask.Flask(__name__)

@app.route('/ping', methods=['GET'])
def ping():
    """Determine if the container is working and healthy. In this sample container, we declare
    it healthy if we can load the model successfully."""
    health = ScoringService.get_model() is not None  # You can insert a health check here

    status = 200 if health else 404
    return flask.Response(response='\n', status=status, mimetype='application/json')


@app.route('/invocations', methods=['POST'])
def predict():
    """Do an inference on a single batch of data. In this sample server, we take data as CSV, convert
    it to a pandas data frame for internal use and then convert the predictions back to CSV (which really
    just means one prediction per line, since there's a single column.
    """
    data = None

    if flask.request.content_type == 'text/plain':
        data = flask.request.data.decode('utf-8')
        s = io.StringIO(data).read()        
    else:
        return flask.Response(response='This predictor only supports text data', status=415, mimetype='text/plain')

    print('input: ' + s)

    # Do the prediction
    prediction = ScoringService.predict(s)

    # Convert from numpy back to CSV
#     out = StringIO.StringIO()
#     pd.DataFrame({'results':predictions}).to_csv(out, header=False, index=False)
#     result = out.getvalue()

    return flask.Response(response=str(prediction), status=200, mimetype='text/plain')

import re
import sqlparse
from typing import List

COMMANDS = ['SELECT', 'INSERT', 'UPDATE', 'DELETE']
TABLES = {'customer', 'lineitem', 'nation', 'orders', 'part', 'partsupp', 'region', 'supplier'}
KEY_COLUMNS = {'c_custkey', 'c_nationkey', 'l_orderkey', 'l_linenumber', 'l_suppkey', 'l_partkey', 'n_nationkey',
               'n_regionkey', 'o_orderkey', 'o_custkey', 'p_partkey', 'ps_partkey', 'ps_suppkey', 'r_regionkey',
               's_suppkey', 's_nationkey'}

COLUMNS = {'c_custkey', 'c_mktsegment', 'c_nationkey', 'c_name', 'c_address', 'c_phone', 'c_acctbal', 'c_comment',
           'l_shipdate', 'l_orderkey', 'l_discount', 'l_extendedprice', 'l_suppkey', 'l_quantity', 'l_returnflag',
           'l_partkey',
           'l_linestatus', 'l_tax', 'l_commitdate', 'l_receiptdate', 'l_shipmode', 'l_linenumber', 'l_shipinstruct',
           'l_comment',
           'n_nationkey', 'n_name', 'n_regionkey', 'n_comment',
           'o_orderdate', 'o_orderkey', 'o_custkey', 'o_orderpriority', 'o_shippriority', 'o_clerk', 'o_orderstatus',
           'o_totalprice', 'o_comment',
           'p_partkey', 'p_type', 'p_size', 'p_brand', 'p_name', 'p_container', 'p_mfgr', 'p_retailprice', 'p_comment',
           'ps_partkey', 'ps_suppkey', 'ps_supplycost', 'ps_availqty', 'ps_comment',
           'r_regionkey', 'r_name', 'r_comment',
           's_suppkey', 's_nationkey', 's_comment', 's_name', 's_address', 's_phone', 's_acctbal'}


def _remove_comments_from_sql(sql: str) -> str:
    """
    Removes comments from SQL query
    @param sql: the query
    @return: query without comments
    """
    # remove /* */ comments
    sql = re.sub(r"\s?/\*.+\*/", "", sql)
    # remove -- comments
    sql = re.sub(r".*--.*", "", sql)
    return sql


def _split_in_tokens(query: str) -> List[str]:
    """
    Splits query into tokens
    @param query: the query
    @return: tokens
    """
    query = _remove_comments_from_sql(query)
    # tokens = re.split(r'( |.|,|\(|\)|\n|\t|\s)\s*', query)
    # tokens = re.split(r'( |,|\.|-|\(|\)|\+|\*|\\|\n|\t|\s)\s*', query)
    # tokens = re.split(r'\p{Punct}', query)
    # to_ignore = {'', ',', '.','-', '(', ')', '\n', '\t', ' ', '+'}
    # [t for t in tokens if t not in to_ignore]
    return re.findall(r'[A-Za-z0-9_]+', query, flags=re.IGNORECASE)


def query_command(query: str):
    """
    Extracts the query command
    @param query: the query
    @return: the query command ('SELECT', 'INSERT', 'UPDATE', 'DELETE', ...) or None if not found
    """
    tokens = _split_in_tokens(query)
    if len(tokens) > 0:
        cmd = tokens[0].upper()
        return cmd
    else:
        return None



def query_tables(query: str):
    """
    Extract the list of tables involved in the query, by matching the tables defined in the data schema
    @param query: the query
    @return: the list of tables or None if not found
    """
    tokens = _split_in_tokens(query)
    tables = set(tokens).intersection(TABLES)
    if len(tables) == 0:
        return None
    else:
        return list(tables)


def n_query_columns(query: str):
    return len(query_columns(query))


def query_columns(query: str):
    """
    Extract the list of columns involved in the query, by matching the columns defined in the data schema
    @param query: the query
    @return: the list of columns or None if not found
    """
    tokens = _split_in_tokens(query)
    columns = set(tokens).intersection(COLUMNS)
    return list(columns)


def _find_tokens(root_token: sqlparse.sql.Statement, condition, continue_if_condition_match=True):
    tokens = []
    condition_match = condition(root_token)
    if condition_match:
        tokens.append(root_token)
    if root_token.is_group and (not condition_match or continue_if_condition_match):
        for t in root_token.tokens:
            tokens = tokens + _find_tokens(t, condition, continue_if_condition_match)

    return tokens


def remove_subquery(root_token: sqlparse.sql.Token):
    # convert tokenized query to string and remove newlines
    token_str = root_token.value.replace('\n', '')
    # remove subquery using regular expression
    token_str_no_subquery = re.sub(r'\([ \t]*SELECT.*FROM.*\)', "", token_str, flags=re.IGNORECASE)

    if token_str == token_str_no_subquery:
        return root_token
    else:
        # re-parse the query without the regular expression
        parsed = sqlparse.parse(token_str_no_subquery)
        return parsed[0]


def match_column_identifier(token: sqlparse.sql.Token):
    return str(token.ttype) == 'Token.Name' or \
           bool(re.match(r'^([A-Za-z0-9_-]+\.)+[A-Za-z0-9_-]+$', token.value))


def query_columns_where_cond(query: str, query_cols: List[str] = None):
    """
    From the list of columns involved in the query, extracts only the ones used inside a WHERE statement
    @param query: the query
    @param query_cols: the list of columns involved in the query
    @return: the list of columns used inside a WHERE statement
    """
    if query_cols is None:
        query_cols = query_columns(query)

    query = _remove_comments_from_sql(query)
    parsed = sqlparse.parse(query)
    where_tokens = _find_tokens(parsed[0], lambda t : isinstance(t, sqlparse.sql.Where))
    all_columns_names = []
    for where_token in where_tokens:
        where_token = remove_subquery(where_token)
        columns_tokens = _find_tokens(where_token, match_column_identifier, continue_if_condition_match=False)
        columns_names = [t.value.split('.')[-1] for t in columns_tokens] # take only the rigth part of table.column
        all_columns_names = all_columns_names + columns_names

    return list(set(all_columns_names).intersection(query_cols))


def query_groupby_conditions(query: str, query_cols: List[str] = None):
    """
        From the list of columns involved in the query, extracts only the ones used inside a WHERE statement
        @param query: the query
        @param query_cols: the list of columns involved in the query
        @return: the list of columns used inside a WHERE statement
        """
    if query_cols is None:
        query_cols = query_columns(query)

    groupby_conditions = 0
    query = _remove_comments_from_sql(query)
    parsed = sqlparse.parse(query)

    if len(parsed) > 0:
        groupby_tokens = _find_tokens(parsed[0], lambda t: t.value.upper() == "GROUP BY")

    for groupby_token in groupby_tokens:
        after_groupby = False
        tokens = [t for t in groupby_token.parent.tokens if not t.is_whitespace]
        for t in tokens:
            if t.value.upper() == "GROUP BY":
                after_groupby = True
            elif after_groupby:
                if isinstance(t, sqlparse.sql.Identifier):
                    groupby_conditions += 1
                    break
                elif isinstance(t, sqlparse.sql.IdentifierList):
                    groupby_conditions += len([t for t in t.tokens if isinstance(t, sqlparse.sql.Identifier)])
                    break

    return groupby_conditions


def query_orderby_conditions(query: str, query_cols: List[str] = None):
    """
        From the list of columns involved in the query, extracts only the ones used inside a WHERE statement
        @param query: the query
        @param query_cols: the list of columns involved in the query
        @return: the list of columns used inside a WHERE statement
        """
    if query_cols is None:
        query_cols = query_columns(query)

    orderby_conditions = 0
    query = _remove_comments_from_sql(query)
    parsed = sqlparse.parse(query)

    if len(parsed) > 0:
        orderby_tokens = _find_tokens(parsed[0], lambda t: t.value.upper() == "ORDER BY")

    for orderby_token in orderby_tokens:
        after_orderby = False
        tokens = [t for t in orderby_token.parent.tokens if not t.is_whitespace]
        for t in tokens:
            if t.value.upper() == "ORDER BY":
                after_orderby = True
            elif after_orderby:
                if isinstance(t, sqlparse.sql.Identifier):
                    orderby_conditions += 1
                    break
                elif isinstance(t, sqlparse.sql.IdentifierList):
                    orderby_conditions += len([t for t in t.tokens if isinstance(t, sqlparse.sql.Identifier)])
                    break

    return orderby_conditions


# def query_columns_join_cond(query: str, query_cols: List[str] = None):
#     """
#         From the list of columns involved in the query, extracts only the ones used inside a WHERE statement
#         @param query: the query
#         @param query_cols: the list of columns involved in the query
#         @return: the list of columns used inside a WHERE statement
#         """
#     if query_cols is None:
#         query_cols = query_columns(query)
#
#     query = _remove_comments_from_sql(query)
#     parsed = sqlparse.parse(query)
#     for token in parsed[0].flatten():
#         if 'JOIN' in token.value.upper():
#             print()


if __name__ == '__main__':
    sql_text = """  
    /* ApplicationName=DataGrip 2020.2.3 */ 
    
    -- using 377385426 as a seed to the RNG
    select
        l_returnflag+3-7/6,
        lineitem.l_linestatus,
        sum(l_quantity) as sum_qty,
        sum(l_extendedprice) as sum_base_price,
        sum(l_extendedprice * (1 - l_discount)) as sum_disc_price,
        sum(l_extendedprice * (1 - l_discount) * (1 + l_tax)) as sum_charge,
        avg(l_quantity) as avg_qty,
        avg(l_extendedprice) as avg_price,
        avg(l_discount) as avg_disc,
        count(*) as count_order
    from
        lineitem
    where
        l_shipdate <= date '1998-12-01' - interval '113' day
    group by
        l_returnflag,
        l_linestatus
    order by
        l_returnflag,
        l_linestatus"""

    sql_text = """
    SELECT l_returnflag,
       l_linestatus,
       l_shipdate
    FROM   lineitem
    WHERE  lineitem.l_linestatus > 30 AND
            NOT EXISTS > ALL (SELECT customer.c_acctbal
                   FROM   customer
                   WHERE  customer.c_custkey > 3000000) as pippo
"""

    sql_text = """
    select
	ps_partkey,
	sum(ps_supplycost * ps_availqty) as value
from
	partsupp,
	supplier,
	nation
where
	ps_suppkey = s_suppkey
	and s_nationkey = n_nationkey
	and n_name = 'FRANCE'
group by
	table.ps_partkey,
	s_suppkey
having
		sum(ps_supplycost * ps_availqty) > (
			select
				sum(ps_supplycost * ps_availqty) * 0.0001000000
			from
				partsupp,
				supplier,
				nation
			where
				ps_suppkey = s_suppkey
				and s_nationkey = n_nationkey
				and n_name = 'FRANCE'
			group by
			    partsupp.s_suppkey
		)
order by
	value desc;"""

    print("command: " + str(query_command(sql_text)))
    print("tables: " + str(query_tables(sql_text)))
    print("columns: " + str(query_columns(sql_text)))
    print("group by columns: " + str(query_groupby_conditions(sql_text)))
    print("group by columns: " + str(query_orderby_conditions(sql_text)))


